# Architectural decision record

## Scope

- Structure
- Functional requirements
- Non-functional requirements
- Dependencies
- Interfaces
- Construction techniques

## Structure

- Title
- Status
  - ${\color{gray}\text{proposed}}$
  - ${\color{green}\text{accepted}}$
  - ${\color{red}\text{supressed}}$
- Context
- Decision
- Argument
- Consequences

## Templates

[link](https://github.com/joelparkerhenderson/architecture-decision-record)
