# Runner

```sh
docker run -d --name gitlab-runner --restart always   -v /opt/gitlab-runner/config:/etc/gitlab-runner   -v /var/run/docker.sock:/var/run/docker.sock   gitlab/gitlab-runner:latest
```

```sh
DESCRIPTION=dind-2 \
REGISTRATION_TOKEN=TOKEN \
MAINTENANCE="Docker gitlab-runner" \
TAG_LIST="docker,dind" \
GITLAB_URL="https://gitlab.example.com/" \
CACHE_TYPE=s3 \
CACHE_PATH="" \
CACHE_SHARED=true \
CACHE_S3_SERVER_ADDRESS=glos.example.com:9000 \
CACHE_S3_ACCESS_KEY=LOGIN-minio \
CACHE_S3_SECRET_KEY=PASSWORD-minio \
CACHE_S3_BUCKET_NAME=gitlab-cache \
CACHE_S3_INSECURE=true \
CACHE_S3_AUTHENTICATION_TYPE=access-key && \
docker run --rm -v /opt/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner register \
--non-interactive \
--executor "docker" \
--docker-volumes "/certs/client" \
--docker-privileged \
--docker-image docker:23.0.1 \
--url "$GITLAB_URL" \
--registration-token "$REGISTRATION_TOKEN" \
--description "$DESCRIPTION" \
--maintenance-note $MAINTENANCE \
--tag-list "$TAG_LIST" \
--run-untagged="true" \
--locked="false" \
--access-level="not_protected" \
--docker-pull-policy="if-not-present" \
--cache-type="$CACHE_TYPE" \
--cache-path="$CACHE_PATH" \
--cache-shared="$CACHE_SHARED" \
--cache-s3-server-address="$CACHE_S3_SERVER_ADDRESS" \
--cache-s3-access-key="$CACHE_S3_ACCESS_KEY" \
--cache-s3-secret-key="$CACHE_S3_SECRET_KEY" \
--cache-s3-bucket-name="$CACHE_S3_BUCKET_NAME" \
--cache-s3-insecure="$CACHE_S3_INSECURE" \
--cache-s3-authentication_type="$CACHE_S3_AUTHENTICATION_TYPE"
```

```sh
DESCRIPTION=runner-1 \
REGISTRATION_TOKEN=TOKEN \
MAINTENANCE="Docker gitlab-runner" \
TAG_LIST="" \
GITLAB_URL="https://gitlab.example.com/" \
CACHE_TYPE=s3 \
CACHE_PATH="" \
CACHE_SHARED=true \
CACHE_S3_SERVER_ADDRESS=glos.example.com:9000 \
CACHE_S3_ACCESS_KEY=LOGIN-minio \
CACHE_S3_SECRET_KEY=PASSWORD-minio \
CACHE_S3_BUCKET_NAME=gitlab-cache \
CACHE_S3_INSECURE=true \
CACHE_S3_AUTHENTICATION_TYPE=access-key && \
docker run --rm -v /opt/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner register \
--non-interactive \
--executor "docker" \
--docker-volumes "/certs/client" \
--docker-image alpine:3.16 \
--url "$GITLAB_URL" \
--registration-token "$REGISTRATION_TOKEN" \
--description "$DESCRIPTION" \
--maintenance-note $MAINTENANCE \
--tag-list "$TAG_LIST" \
--run-untagged="true" \
--locked="false" \
--access-level="not_protected" \
--docker-pull-policy="if-not-present" \
--cache-type="$CACHE_TYPE" \
--cache-path="$CACHE_PATH" \
--cache-shared="$CACHE_SHARED" \
--cache-s3-server-address="$CACHE_S3_SERVER_ADDRESS" \
--cache-s3-access-key="$CACHE_S3_ACCESS_KEY" \
--cache-s3-secret-key="$CACHE_S3_SECRET_KEY" \
--cache-s3-bucket-name="$CACHE_S3_BUCKET_NAME" \
--cache-s3-insecure="$CACHE_S3_INSECURE" \
--cache-s3-authentication_type="$CACHE_S3_AUTHENTICATION_TYPE"
```
