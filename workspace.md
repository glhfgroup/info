# Подготовка рабочего места

## Установка Xcode

[Источник](https://developer.apple.com/xcode/)

Вместе с Xcode будет установлен git

```sh
git --version
```

Провеьте установку приложений

    ```sh
    python3 --version
    ruby --version
    vim --version
    ```

## Установка homebrew

[Источник](https://docs.brew.sh/Installation)

1.  Скачайте пакет и выполните установку

        ```sh
        /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
        ```

2.  Добавьте путь к исполняемым файлам в переменную окружения PATH

        ```sh
        sudo -i
        echo "/opt/homebrew/bin" > /etc/paths.d/brew
        ```

3.  Переоткройте терминал и проверьте установку

        ```sh
        brew --version
        ```

## Установка Golang

[Источник](https://go.dev/doc/install)

1.  Скачайте пакет и выполните установку
2.  Добавьте путь к исполняемым файлам в переменную окружения PATH

        ```sh
        sudo -i
        echo "/usr/local/go/bin" > /etc/paths.d/go
        echo "/User/user_name/go/bin" > /etc/paths.d/go_home
        ```

3.  Переоткройте терминал и проверьте установку

        ```sh
        go version
        ```

## Установка nodejs

[Источник](https://nodejs.org/en/download/)

Скачайте и установите `macOS Installer (.pkg)` LTS версии

    ```sh
    node --version
    npm --version
    ```

Установите yarn

    ```sh
    sudo npm i -g yarn
    yarn --version
    ```

## Установка VSCode

[Источник](https://code.visualstudio.com/docs/setup/mac)

1.  Скачайте дистрибутив VSCode
2.  Откройте файл, перетащите `Visual Studio Code.app` в `Application`
3.  Закрепите VSCode в панели `Dock`
4.  Запустите VSCode
5.  Откройте палитру команд (Shift+Cmd+P), введите `shell command`, выберите `Shell Command: Install 'code' command in PATH`
6.  Переоткройте терминал, проверьте работу команды `code .`

        ```sh
        code --version
        ```

## Установка Docker desktop

[Источник](https://docs.docker.com/desktop/install/mac-install/)

1.  Скачайте дистрибутив `Docker Desktop for Mac`
2.  Откройте файл, перетащите `Docker.app` в `Application`
3.  Docker compose и kubectl устанавливается вместе с Docker desktop, в меню `General` активируйте `Use Docker Compose V2`
    [Compose specification](https://docs.docker.com/compose/compose-file/)

        ```sh
        docker --version
        docker compose version
        kubectl version
        ```

## Устновка ohmyzsh

[Источник](https://github.com/ohmyzsh/ohmyzsh/wiki)

    ```sh
    echo "alias ll=\"ls -lGa\"" >> ~/.zshrc
    ```

## Установка telnet

    ```sh
    brew install telnet
    ```

## Установка ansible

    ```sh
    brew install ansible
    brew install hudochenkov/sshpass/sshpass
    ansible --version
    sshpass -V
    ```

## Установка Taskfile

[Источник](https://taskfile.dev/installation/)

    ```sh
    brew install go-task/tap/go-task
    task --version
    ```

## Установка goReleaser

[Источник](https://goreleaser.com/install/)

    ```sh
    brew install goreleaser
    goreleaser --version
    ```

## Установка Graphviz

[Источник](https://graphviz.org/download/)

    ```sh
    brew install graphviz
    ```

## Установка MacPorts

[Источник](https://www.macports.org/install.php)

1.  Скачайте пакет `macOS Package (.pkg)`
2.  Установите приложение
3.  Переоткройте терминал и проверьте установку

    ```sh
    port version
    ```
