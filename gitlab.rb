################################################################################
################################################################################
##                Configuration Settings for GitLab CE and EE                 ##
################################################################################
################################################################################

external_url 'https://gitlab.example.com'
# gitlab_rails['gitlab_ssh_host'] = 'ssh.host_example.com'
# gitlab_rails['gitlab_ssh_user'] = ''
gitlab_rails['time_zone'] = 'UTC'

### GitLab email server settings
gitlab_rails['smtp_enable'] = true
gitlab_rails['smtp_address'] = "smtp.gmail.com"
gitlab_rails['smtp_port'] = 587
gitlab_rails['smtp_user_name'] = "robot@example.com"
gitlab_rails['smtp_password'] = "PASSWORD"
gitlab_rails['smtp_domain'] = "example.com"
gitlab_rails['smtp_authentication'] = "login"
gitlab_rails['smtp_enable_starttls_auto'] = true
gitlab_rails['smtp_tls'] = false
gitlab_rails['smtp_pool'] = false

### Email Settings
gitlab_rails['gitlab_email_enabled'] = true
gitlab_rails['gitlab_email_from'] = 'robot@example.com'
gitlab_rails['gitlab_email_display_name'] = 'Robot example com'
gitlab_rails['gitlab_email_reply_to'] = 'robot@example.com'

### GitLab user privileges
gitlab_rails['gitlab_default_can_create_group'] = true
gitlab_rails['gitlab_username_changing_enabled'] = true

### Default Theme
gitlab_rails['gitlab_default_theme'] = 3

### Default project feature settings
gitlab_rails['gitlab_default_projects_features_issues'] = true
gitlab_rails['gitlab_default_projects_features_merge_requests'] = true
gitlab_rails['gitlab_default_projects_features_wiki'] = true
gitlab_rails['gitlab_default_projects_features_snippets'] = true
gitlab_rails['gitlab_default_projects_features_builds'] = true
gitlab_rails['gitlab_default_projects_features_container_registry'] = true

### Automatic issue closing
gitlab_rails['gitlab_issue_closing_pattern'] = "\b((?:[Cc]los(?:e[sd]?|ing)|\b[Ff]ix(?:e[sd]|ing)?|\b[Rr]esolv(?:e[sd]?|ing)|\b[Ii]mplement(?:s|ed|ing)?)(:?) +(?:(?:issues? +)?%{issue_ref}(?:(?:, *| +and +)?)|([A-Z][A-Z0-9_]+-\d+))+)"

### Download location
gitlab_rails['gitlab_repository_downloads_path'] = 'tmp/repositories'

### Gravatar Settings
gitlab_rails['gravatar_plain_url'] = 'http://www.gravatar.com/avatar/%{hash}?s=%{size}&d=identicon'
gitlab_rails['gravatar_ssl_url'] = 'https://secure.gravatar.com/avatar/%{hash}?s=%{size}&d=identicon'

### Webhook Settings
# gitlab_rails['webhook_timeout'] = 10

### GraphQL Settings
# gitlab_rails['graphql_timeout'] = 30

### Allowed hosts
# gitlab_rails['allowed_hosts'] = []

### Monitoring settings
# gitlab_rails['monitoring_whitelist'] = ['127.0.0.0/8', '::1/128']

### Shutdown settings
gitlab_rails['shutdown_blackout_seconds'] = 10

### Consolidated (simplified) object storage configuration
gitlab_rails['object_store']['enabled'] = true
gitlab_rails['object_store']['proxy_download'] = true
gitlab_rails['object_store']['connection'] = {
  'provider' => 'AWS',
  'region' => 'eu-central-1',
  'regionendpoint' => 'glos.example.com:9000',
  'aws_access_key_id' => 'LOGIN-minio',
  'aws_secret_access_key' => 'PASSWORD-minio',
  'path_style' => true
}
gitlab_rails['object_store']['storage_options'] = {}
gitlab_rails['object_store']['objects']['artifacts']['bucket'] = 'gitlab-artifacts'
gitlab_rails['object_store']['objects']['external_diffs']['bucket'] = 'gitlab-mr-diffs'
gitlab_rails['object_store']['objects']['lfs']['bucket'] = 'gitlab-lfs'
gitlab_rails['object_store']['objects']['uploads']['bucket'] = 'gitlab-uploads'
gitlab_rails['object_store']['objects']['packages']['bucket'] = 'gitlab-packages'
gitlab_rails['object_store']['objects']['dependency_proxy']['bucket'] = 'gitlab-dependency-proxy'
gitlab_rails['object_store']['objects']['terraform_state']['bucket'] = 'gitlab-terraform-state'
gitlab_rails['object_store']['objects']['ci_secure_files']['bucket'] = 'gitlab-ci-secure-files'
gitlab_rails['object_store']['objects']['pages']['bucket'] = 'gitlab-pages'

### Backup Settings
# gitlab_rails['manage_backup_path'] = true
# gitlab_rails['backup_path'] = "/var/opt/gitlab/backups"
# gitlab_rails['backup_gitaly_backup_path'] = "/opt/gitlab/embedded/bin/gitaly-backup"
# gitlab_rails['backup_archive_permissions'] = 0644
# gitlab_rails['backup_pg_schema'] = 'public'
# gitlab_rails['backup_keep_time'] = 604800
# gitlab_rails['backup_upload_connection'] = {
#   'provider' => 'AWS',
#   'region' => 'eu-west-1',
#   'aws_access_key_id' => 'AKIAKIAKI',
#   'aws_secret_access_key' => 'secret123',
#   # # If IAM profile use is enabled, remove aws_access_key_id and aws_secret_access_key
#   'use_iam_profile' => false
# }
# gitlab_rails['backup_upload_remote_directory'] = 'my.s3.bucket'
# gitlab_rails['backup_multipart_chunk_size'] = 104857600

### GitLab Shell settings for GitLab
gitlab_rails['gitlab_shell_ssh_port'] = 22
gitlab_rails['gitlab_shell_git_timeout'] = 800

#### Change the initial default admin password and shared runner registration tokens.
gitlab_rails['initial_root_password'] = "PASSWORD"
# gitlab_rails['initial_shared_runners_registration_token'] = "token"

#### Toggle if root password should be printed to STDOUT during initialization
gitlab_rails['display_initial_root_password'] = true

#### Toggle if initial root password should be written to /etc/gitlab/initial_root_password
gitlab_rails['store_initial_root_password'] = true

#### Enable or disable automatic database migrations
gitlab_rails['auto_migrate'] = true

################################################################################
## Container Registry settings
################################################################################

registry_external_url 'https://cr.example.com'

### Settings used by GitLab application
gitlab_rails['registry_enabled'] = true
gitlab_rails['registry_host'] = "cr.example.com"

### Settings used by Registry application
registry['enable'] = true

### Registry backend storage
registry['storage'] = {
  's3' => {
    'accesskey' => 'LOGIN-minio',
    'secretkey' => 'PASSWORD-minio',
    'bucket' => 'containers',
    'region' => 'eu-central-1',
    'regionendpoint' => 'glos.example.com:9000'
  },
  'redirect' => {
    'disable' => false
  }
}

################################################################################
## CI_JOB_JWT
################################################################################
##! RSA private key used to sign CI_JOB_JWT
# gitlab_rails['ci_jwt_signing_key'] = nil # Will be generated if not set.

################################################################################
## GitLab NGINX
################################################################################

nginx['enable'] = true
nginx['client_max_body_size'] = '250m'
nginx['redirect_http_to_https'] = true

################################################################################
## GitLab Pages
################################################################################

pages_external_url "http://pages.example.com"
gitlab_pages['enable'] = true

################################################################################
## GitLab Pages NGINX
################################################################################

pages_nginx['enable'] = true

################################################################################
## Registry NGINX
################################################################################

registry_nginx['enable'] = true
registry_nginx['proxy_set_headers'] = {
 "Host" => "$http_host",
 "X-Real-IP" => "$remote_addr",
 "X-Forwarded-For" => "$proxy_add_x_forwarded_for",
 "X-Forwarded-Proto" => "https",
 "X-Forwarded-Ssl" => "on"
}

################################################################################
## Prometheus Node Exporter
################################################################################

node_exporter['enable'] = true

################################################################################
# Let's Encrypt integration
################################################################################

letsencrypt['enable'] = true
letsencrypt['contact_emails'] = ['robot@example.com']
letsencrypt['auto_renew'] = true
letsencrypt['auto_renew_hour'] = "3"
letsencrypt['auto_renew_minute'] = "30"
letsencrypt['auto_renew_day_of_month'] = "*/7"
