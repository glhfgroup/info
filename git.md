# Подготовка репозитория git

## Настройка подключения

1. [Сгенерируйте ключевую пару ssh](https://docs.gitlab.com/ee/user/ssh.html) (macOS)

   ```sh
   ssh-keygen -t ed25519 -C "<comment>"

   Generating public/private ed25519 key pair.
   Enter file in which to save the key (/home/user/.ssh/id_ed25519):
   # Enter

   Enter passphrase (empty for no passphrase):
   # Enter
   Enter same passphrase again:
   # Enter

   tr -d '\n' < ~/.ssh/id_ed25519.pub | pbcopy
   ```

2. Авторизуйтесь в GitLab
3. Откройте настройки в меню аватара, `Preferences`
4. В левом меню выберите `SSH Keys`
5. Вставьте в поле `Key` содержимое файла `id_ed25519.pub`
6. Введите описание в поле `Title`
7. установите дату истечения ключа, если это необходимо
8. Нажмите `Add key`
9. Проверьте подключение

   ```sh
   ssh -T git@gitlab.example.com
   # Welcome to GitLab, @username!
   ```

## Инициализация репозитория

1. Установите глобальные параметры пользователя и электронной почты

   ```sh
   git config --global user.name "User name"
   git config --global user.email Email
   ```

2. Создайте каталог проекта и перейдите в него

   ```sh
   mkdir project
   cd project
   ```

3. Инициализируйте репозиторий

   ```sh
   git init
   ```

4. Переключитесь на нужную ветку, если это необходимо

   ```sh
   git switch -c trunk
   ```

5. Укажите удаленный источник

   ```sh
   git remote add origin git@gitlab.com:<GROUP | USER>/<REPO>.git
   ```

6. Создайте файл `README.md`, добавьте его, выполните коммит и отправьте изменения в репозиторий

   ```sh
   touch README.md
   git add .
   git commit -m "Initial commit"
   git push -u origin trunk
   ```
