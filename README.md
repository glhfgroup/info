# Installation gitlab

Example gitlab installation with container registry, minio s3, kroki and runners

```sh
export GITLAB_HOME=/srv/gitlab
```

```sh
export GITLAB_HOME=$HOME/gitlab
```

## google mail account

2FA: Применить [двухэтапную аутентификацию](https://myaccount.google.com/signinoptions/two-step-verification/enroll-welcome) и добавить [пароль приложения](https://myaccount.google.com/apppasswords)

или

LSA: Разрешить [ненадежные приложения](https://myaccount.google.com/u/1/lesssecureapps)
